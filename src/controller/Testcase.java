package controller;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.BankAccount;
import view.InvestmentFrame;

public class Testcase {
	BankAccount bank1;    //��С�Ȩͧ������������¡�� class BankAccount , method � class BankAccount
	InvestmentFrame frame;  // ��С�Ȩͧ������������¡ class InvestmentFrame,  method � class InvestmentFrame �һ����ż��ʴ����͡�ҧ˹�Ҩ�
	ActionListener listener;
	
	public static void main(String[] args) {
		new Testcase();
	}
	
	public Testcase() {	
		frame = new InvestmentFrame();
		listener = new AddInterestListener();
		frame.setVisible(true);
		frame.setSize(450, 100);
		frame.setListener(listener);
		setTestCase();
	}
	
	public void setTestCase(){	
		bank1 = new BankAccount(1000);
		frame.setBalance(bank1.getBalance());
	}
	
	 class AddInterestListener implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {
				double rate = Double.parseDouble(frame.getInput());
	       		double interest = bank1.getBalance() * rate / 100;
	       		bank1.deposit(interest);
	       		frame.setBalance(bank1.getBalance());
			}
	}
	  
}
