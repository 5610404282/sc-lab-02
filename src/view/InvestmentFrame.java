package view;


import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class InvestmentFrame extends JFrame
{    
   private static final double DEFAULT_RATE = 5;

   private JLabel rateLabel;
   private JTextField rateField;
   private JButton button;
   private JLabel resultLabel;
   private JPanel panel; 
   
   public InvestmentFrame(){
      createFrame();
      createPanel();
   }

   private void createFrame(){  //���ҧ JLabel ,JTextField ,JButton
      rateLabel = new JLabel("Interest Rate: ");
      rateField = new JTextField(10);
      rateField.setText("" + DEFAULT_RATE);
      resultLabel = new JLabel();
      button = new JButton("Add Interest");
   }
   
   private void createPanel(){  // ��� rateLabel,rateField,button,resultLabe  ŧ� panel
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);
      add(panel);
   } 
   
   public void setListener(ActionListener list) {  
		button.addActionListener(list);
	}
   public String getInput(){ // �Ѻ��� input ����������
		return rateField.getText();
   }
   public void setBalance(double d){ // �ʴ� balance �� resultLabel
		resultLabel.setText("balance: " +d);
   }
}
